# Debian Package Build

## Introduction

This project contains a tool to help creating Debian Packages. dpb
allows to build and rebuild a git project into a Debian package in a
clean and discartable environment. By bootstrapping the environment,
this assures that the dependencies are correct and the code is compiled
within the correct environment.

## How to Build

Note, running tpb should be done in the directory containing the
'debian' directory. It is preferred that this is the top level git
project location, though the tool has been adjusted to allow operating
if this is not the case (e.g. not a git location or the top git location
is further upwards).


A number of examples:

* Build the local environment. Do not upload.

```shell
$ debian-package-build
```

* Build the local environment, tag the build with a new version and
generate the correct changelog (consisting out of the short git messages)

```shell
$ debian-package-build -c
```

* Build the local environment, do not upload, but build for 32-bit
(when the host is 64 bit)

```shell
$ debian-package-build -a i386
```

* Build the local environment, build for Debian 9 (default is Debian 10
at the time of writing)

```shell
$ debian-package-build -r stretch
```

It is also possible to upload complex packages without compiling them locally:

```shell
$ debian-package-build -uc --source-only
```

## Profiles

The script uses _profiles_ to build for different Debian distributions. At
the moment of writing, The profiles for ubuntu and Debian are available
(Debian is the default).

Build for ubuntu:

```
$ debian-package-build --profile=ubuntu
```

There are also short-hand wrappers to use for Ubuntu and Debian:

```
$ upb
$ dpb
```

Extra configuration can be added in a configuration file in the $HOME
that will be loaded after the global profile has been read:

```
$ cat ~/.debian-package-build/profiles/sid
export DEBFULLNAME="Marc Leeman"
export DEBEMAIL=marc.leeman@gmail.com
```

## Package to Repository

debian-package-to-repo (part of debian-package-build) has been made to
take any selection of packages and provide you with a self-extracting
repository: it takes care of the the dependencies and allows you to
install whatever you want.

There are a limited number of things that need to be taken into account:

* You need to know the architecture that you want to debug: old PC2
  systems are 32 bit (i386), all other systems are currently 64-bit
  (amd64)

* You need to know the target release, this is either Debian 10 (buster)
  or Debian 11 (bullseye). If you are uncertain, have a look at the login
  of the system, or /etc/os-release is also a good point to start.

* Finally, you need to know what packages you want.

After that, just run (to get the addon with telnet, tcdpdump, traceroute
and nmap):

```shell
$ sudo debian-package-to-repo -a amd64 -r bullseye -p telnet,tcpdump,traceroute,nmap
```


The result will be a run file:

```
debian-add-on-telnet-tcpdump-traceroute-nmap-bullseye-stable_amd64_2022-01-26-123339.run
```

In order to install it, copy it to your target (best on the data disk
in /data/ in order to keep memory free) and run it:

```shell
$ sudo ./debian-add-on-telnet-tcpdump-traceroute-nmap-bullseye-stable_amd64_2022-01-26-123339.run
```

For easiness, I will share the above run files for Debian buster/bullseye
and i386/amd64. I have not tested all of them, so let me know should
there be a problem.


```shell
for a in i386 amd64; do
  for r in buster bullseye sid; do \
    sudo debian-package-to-repo -a "${a}" -r "${r}" -p telnet,tcpdump,traceroute,nmap; \
  done; \
done
```

debian-package-to-repo runs independently from the apt configuration
on the host machine: it connects to deb.debian.org and downloads the
packages.
